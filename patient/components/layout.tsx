import React, {useEffect, useState, useContext} from 'react'
import Image from 'next/image'
import logo from '../public/logo.png'
import { Button, Card, Col, Container, Form, Nav, Navbar, NavDropdown, Row, Table } from 'react-bootstrap'
import ConsultationRoom from '../features/consultationroom/ConsultationRoom'
import WaitingRoom from '../features/waitingroom/WaitingRoom'
var FontAwesome = require('react-fontawesome')

import { useAppSelector, useAppDispatch } from '../app/hooks'
import {
  enterWaitingRoom,
  exitWaitingRoom,
  getUser,
  getIsSubmit,
} from '../features/waitingroom/waitingRoomSlice'

export default function Layout({
  children
}: {
  children: React.ReactNode
}) {
  const isSubmit = useAppSelector(getIsSubmit)

  return (
    <>
      <Navbar className="border-bottom">
        <Container>
          <Navbar.Brand href="#home">
            <Image
              src={logo}
              className="d-inline-block align-top"
              height={26}
              width={80}
            />
          </Navbar.Brand>
          <Nav
            className="ms-auto my-2 my-lg-0"
            style={{ maxHeight: '100px' }}
            navbarScroll
          >
            <Nav.Link href="https://toan.vsee.me/test_computer">
              <FontAwesome
                name="desktop"
              /> Test Computer
            </Nav.Link>
          </Nav>
        </Container>
      </Navbar>
      <Container>
        <Row className="justify-content-center my-5">
          <Col md="6">
            {
              !isSubmit
              ? (
                  <WaitingRoom />
                )
              : (
                  <ConsultationRoom />
                )
            }
          </Col>
        </Row>
        <footer className="text-center"><small>© 2017 <a href="https://vsee.com/" className="text-success">VSee</a></small></footer>
      </Container>
    </>
  )
}