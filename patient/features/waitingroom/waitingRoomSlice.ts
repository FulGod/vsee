import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'

import type { AppState, AppThunk } from '../../app/store'

export interface WaitingRoomState {
  isSubmit: true | false
  user: { name: string, reason: string, vseeid: string, entertime: number }
}

const initialState: WaitingRoomState = {
  isSubmit: false,
  user: { name: "", reason: "", vseeid: "", entertime: 0 },
}

export const waitingRoomSlice = createSlice({
  name: 'waitingRoom',
  initialState,
  reducers: {
    enterWaitingRoom: (state, action) => {
      state.user = action.payload
      state.isSubmit = true
    },
    exitWaitingRoom: (state) => {
      state.user = { name: "", reason: "", vseeid: "", entertime: 0 }
      state.isSubmit = false
    }
  }
})

export const { enterWaitingRoom, exitWaitingRoom } = waitingRoomSlice.actions

export const getIsSubmit = (state: AppState) => state.waitingRoom.isSubmit
export const getUser = (state: AppState) => state.waitingRoom.user

export default waitingRoomSlice.reducer
