import React, {useEffect, useState, useContext} from 'react'
import { useForm } from 'react-hook-form'

import { Button, Card, Col, Container, Form, Nav, Navbar, NavDropdown, Row, Table } from 'react-bootstrap'
var FontAwesome = require('react-fontawesome')

import { useAppSelector, useAppDispatch } from '../../app/hooks'
import {
  enterWaitingRoom,
  exitWaitingRoom,
  getUser,
  getIsSubmit,
} from './waitingRoomSlice'

const WaitingRoom = () => {
  const dispatch = useAppDispatch()
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm()
  const onSubmit = ({ name, reason, vseeid }) => {
    dispatch(enterWaitingRoom({ name: name, reason: reason, vseeid: vseeid, entertime: Date.now() }))
  }

  return (
    <Card
      border="success"
      className="mb-2"
    >
      <Card.Header className="bg-success fw-bold text-white">
        <FontAwesome
            name="video-camera"
          /> Talk to Toan Nguyen
      </Card.Header>
      <Card.Body>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Form.Group className="mb-3" controlId="yourName">
            <Form.Label>Please fill in your name to proceed<Form.Text className="text-danger">*</Form.Text></Form.Label>
            <Form.Control type="text" placeholder="Your Name" required {...register('name')} />
          </Form.Group>

          <Form.Group className="mb-3" controlId="yourReason">
            <Form.Label>Reason for visit <Form.Text muted>(optional)</Form.Text></Form.Label>
            <Form.Control as="textarea" rows={2} placeholder="Your reason for visit" {...register('reason')} />
          </Form.Group>

          <Form.Group className="mb-3" controlId="yourVseeId">
            <Form.Label>VSee ID<Form.Text className="text-danger">*</Form.Text></Form.Label>
            <Form.Control type="email" placeholder="your vsee id for doctor to call" required {...register('vseeid')} />
          </Form.Group>

          <Button variant="warning" type="submit" className="text-white">
            Enter Waiting Room
          </Button>
        </Form>
      </Card.Body>
    </Card>
  )
}

export default WaitingRoom;
