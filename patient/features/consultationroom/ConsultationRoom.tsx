import React, {useEffect, useState, useContext} from 'react'

import { Button, Card, Col, Container, Form, Nav, Navbar, NavDropdown, Row, Table } from 'react-bootstrap'
var FontAwesome = require('react-fontawesome')
import PubNub from 'pubnub';
import { PubNubProvider, usePubNub } from 'pubnub-react';
import keyConfiguration from "../../config/pubnub-keys.json";

import { useAppSelector, useAppDispatch } from '../../app/hooks'
import {
  enterWaitingRoom,
  exitWaitingRoom,
  getUser,
  getIsSubmit,
} from '../waitingroom/waitingRoomSlice'

const ConsultationRoom = () => {
  const dispatch = useAppDispatch()
  const [message, setMessage] = useState('Your provider will be with you shortly')
  const user = useAppSelector(getUser)
  const pubnub = new PubNub({
    publishKey: keyConfiguration.publishKey,
    subscribeKey: keyConfiguration.subscribeKey,
    uuid: user.vseeid
  })
  const [doctorUuid] = useState("toan");
  const [rootChannel] = useState("fulgod")
  const [channels] = useState([rootChannel, user.vseeid]);
  pubnub.setState(
    {
      state: {user},
      channels: channels
    },
    function (status, response) {
      // console.log(response);
    }
  );
  useEffect(() => {
    try {
      pubnub.addListener({
        presence: function(event) {
          // console.log(event);
          var channelName = event.channel;
          if (event.uuid == doctorUuid) {
            if (event.action == "state-change") {
              channelName = event.state.channel;
            }
            if (channelName == rootChannel) {
              setMessage('Your provider will be with you shortly');
            } else if (channelName == user.vseeid) {
              setMessage('The visit is in progress');
            } else {
              setMessage('Doctor is currently busy and will attend to you soon');
            }
          }
        }
      });
      pubnub.hereNow(
        {
          channels: [rootChannel],
          includeState: true,
          includeUUIDs: true
        },
        function (status, response) {
          console.log(response);
          var occupancy = response.channels[rootChannel].occupancy;
          var occupants = response.channels[rootChannel].occupants;
          for (var i = 0; i < occupancy; i++) {
            var occupant = occupants[i];
            if (occupant.uuid == doctorUuid) {
              if (occupant.state == undefined || occupant.state.channel == rootChannel) {
                setMessage('Your provider will be with you shortly');
              } else if (occupant.state.channel == user.vseeid) {
                setMessage('The visit is in progress');
              } else {
                console.log('hereNow');
                setMessage('Doctor is currently busy and will attend to you soon');
              }
            }
          }
        }
      );
      pubnub.subscribe({
        channels: channels,
        withPresence: true, 
      });
    } catch (e) {
      console.log(e);
    }
  }, [pubnub, channels]);

  const onSubmit = () => {
    dispatch(exitWaitingRoom())
    pubnub.unsubscribeAll()
  }

  return (
    <PubNubProvider client={pubnub}>
      <Card
        border="success"
        className="mb-2"
      >
        <Card.Header className="bg-success fw-bold text-white">
          <FontAwesome
            name="clock-o"
          /> Connecting with your provider
        </Card.Header>
        <Card.Body className="text-center">
          <Form onSubmit={onSubmit}>
            <h5 className="mt-5">{message}</h5>

            <Button variant="warning" type="submit" className="text-white mt-4 mb-5">
              Exit Waiting Room
            </Button>

            <p className="border-top pt-3">
              <small>If you close the video conferenceby mistake please, <a href="#" className="text-success">click here to relaunch video</a> again.</small>
            </p>
          </Form>
        </Card.Body>
      </Card>
    </PubNubProvider>
  )
}

export default ConsultationRoom;
