import React, {useEffect, useState, useContext} from 'react'
import PubNub from 'pubnub';
import { PubNubProvider, usePubNub } from 'pubnub-react';
import keyConfiguration from "../../config/pubnub-keys.json";

import { Button, Card, Col, Container, Form, Nav, Navbar, NavDropdown, Row, Table } from 'react-bootstrap'
var FontAwesome = require('react-fontawesome')

import { useAppSelector, useAppDispatch } from '../../app/hooks'
import {
  addPatient,
  removePatient,
  updateChannel,
  getChannel,
  getPatients,
  getPatientStates,
} from './waitingRoomSlice'

import PatientList from '../../components/PatientList'


const WaitingRoom = () => {
  const dispatch = useAppDispatch();
  const [uuid] = useState("toan");
  const [rootChannel] = useState("fulgod")
  const [channels] = useState([rootChannel]);
  const currentChannel = useAppSelector(getChannel);
  const patients = useAppSelector(getPatients);
  const patientStates = useAppSelector(getPatientStates);

  const pubnub = new PubNub({
    publishKey: keyConfiguration.publishKey,
    subscribeKey: keyConfiguration.subscribeKey,
    uuid: uuid
  });

  useEffect(() => {
    try {
      pubnub.addListener({
        presence: function(event) {
          if (event.action == "state-change" && event.uuid != uuid) {
            dispatch(addPatient(event));
            // console.log(event);
          }
          if ((event.action == "timeout") || (event.action == "leave") && event.uuid != uuid) {
            if (event.channel == currentChannel) {
              pubnub.setState(
                {
                  state: {channel: rootChannel},
                  channels: [rootChannel]
                },
                function (status, response) {
                  // console.log(response);
                }
              );
              pubnub.unsubscribe({
                channels: [currentChannel]
              });
              dispatch(updateChannel(rootChannel));
            }
            dispatch(removePatient(event.uuid));
            // console.log(event);
          }
        }
      });

      pubnub.hereNow(
        {
          channels: channels,
          includeState: true,
          includeUUIDs: true
        },
        function (status, response) {
          console.log(response);
          var occupancy = response.channels[rootChannel].occupancy;
          var occupants = response.channels[rootChannel].occupants;
          var occupantIsOnline = false;
          var patients = [];
          var patientTimes = [];
          for (var i = 0; i < occupancy; i++) {
            var occupant = occupants[i];
            if (occupant.uuid !== uuid && occupant.state !== undefined) {
              patientTimes.push(occupant.state.user.entertime);
              patients[occupant.state.user.entertime] = occupant;
              // dispatch(addPatient(occupant));
            }
            if (occupant.uuid == currentChannel) {
              occupantIsOnline = true;
            }
          }
          patientTimes.sort();
          patientTimes.forEach((value) => {
              dispatch(addPatient(patients[value]));
          });
          if (!occupantIsOnline) {
            pubnub.setState(
              {
                state: {channel: rootChannel},
                channels: [rootChannel]
              },
              function (status, response) {
                // console.log(response);
              }
            );
            pubnub.unsubscribe({
              channels: [currentChannel]
            });
            pubnub.subscribe({ 
              channels: [rootChannel],
              withPresence: true
            });
            dispatch(updateChannel(rootChannel));
          }
        }
      );

      pubnub.subscribe({ 
        channels: channels,
        withPresence: true
      });
    } catch (e) {
      console.log(e);
    }
  }, [pubnub, channels]);

  return (
    <PubNubProvider client={pubnub}>
      <Card
        border="success"
        className="mb-2"
      >
        <Card.Header className="bg-success fw-bold text-white">
          <div className="d-flex align-items-center">
            <FontAwesome
                name="bars"
              /><span className="ms-2">Waiting Room</span>
            <FontAwesome
                name="user-plus"
                className="ms-auto"
              /><span className="ms-2">Invite people</span>
          </div>
        </Card.Header>
        <Card.Body>
          <Table responsive borderless>
            <tbody>
              {
                patients.length > 0
                ?
                  patients.map((uuid) => (
                    // <React.Fragment key={uuid}>
                    <PatientList key={uuid} patient={patientStates[uuid]} />
                    // </React.Fragment>
                  ))
                :
                  <tr className="text-center fw-bolc">
                    <td colSpan={4}>There are no patients waiting at this time.</td>
                  </tr>
              }
            </tbody>
          </Table>
        </Card.Body>
      </Card>
    </PubNubProvider>
  )
}

export default WaitingRoom;
