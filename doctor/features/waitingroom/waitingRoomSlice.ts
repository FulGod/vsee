import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'

import type { AppState, AppThunk } from '../../app/store'


type patientStates = {
  [key:string]: { name: string, reason: string, vseeid: string, entertime: number };
}
export interface WaitingRoomState {
  channel: string,
  patients: string[],
  patientStates: patientStates
}

const initialState: WaitingRoomState = {
  channel: "fulgod",
  patients: [],
  patientStates: {}
}

export const waitingRoomSlice = createSlice({
  name: 'waitingRoom',
  initialState,
  reducers: {
    addPatient: (state, action) => {
      const uuid : string = action.payload.uuid
      if (!state.patients.includes(uuid) && action.payload.state !== undefined) {
        state.patients.push(uuid)
        state.patientStates[uuid] = action.payload.state
      }
    },
    removePatient: (state, action) => {
      var index = state.patients.indexOf(action.payload)
      if (index !== -1) {
        state.patients.splice(index, 1)
      }
    },
    updateChannel: (state, action) => {
      state.channel = action.payload
    }
  }
})

export const { addPatient, removePatient, updateChannel } = waitingRoomSlice.actions

export const getChannel = (state: AppState) => state.waitingRoom.channel
export const getPatients = (state: AppState) => state.waitingRoom.patients
export const getPatientStates = (state: AppState) => state.waitingRoom.patientStates

export default waitingRoomSlice.reducer
