import React, {useEffect, useState, useContext} from 'react'
import { differenceInMinutes } from 'date-fns'
import { PubNubProvider, usePubNub } from 'pubnub-react';
import Button from 'react-bootstrap/Button'
var FontAwesome = require('react-fontawesome')

import { useAppSelector, useAppDispatch } from '../app/hooks'
import {
  addPatient,
  removePatient,
  updateChannel,
  getChannel,
  getPatients,
  getPatientStates,
} from '../features/waitingroom/waitingRoomSlice'

const PatientList = ({
  patient
}: {
  patient: { user: { name: string, reason: string, vseeid: string, entertime: number } }
}) => {
  const dispatch = useAppDispatch()
  const pubnub = usePubNub()
  const [rootChannel] = useState("fulgod")
  const currentChannel = useAppSelector(getChannel);
  const chatUser = () => {
    if (currentChannel != rootChannel) {
      pubnub.unsubscribe({
        channels: [currentChannel]
      });
    }
    pubnub.subscribe({
      channels: [rootChannel, patient.user.vseeid],
      withPresence: true, 
    });
    pubnub.setState(
      {
        state: {channel: patient.user.vseeid},
        channels: [rootChannel]
      },
      function (status, response) {
        console.log(response);
      }
    );
    dispatch(updateChannel(patient.user.vseeid));
  }
  const callUser = () => {
    chatUser()
    document.location.href = "vsee:"+patient.user.vseeid
  }

  return (
    <tr>
      <td>
        <FontAwesome
          name="user"
          size="3x"
        />
      </td>
      <td>
        <h6>{patient.user.name}</h6>
        <div className="text-muted">{patient.user.reason}</div>
        <div className="text-muted">{patient.user.vseeid}</div>
      </td>
      <td>
        <div>
          <FontAwesome
            name="video-camera"
            className="text-success"
          /> Online
        </div>
        <div>
          <FontAwesome
            name="clock-o"
          /> Waiting: {differenceInMinutes(Date.now(), patient.user.entertime)} min
        </div>
      </td>
      <td>
        <Button variant="warning" onClick={chatUser} className="ms-1 text-white">
          <FontAwesome
            name="comment"
          />
        </Button>
        <Button variant="warning" onClick={callUser} className="ms-1 text-white">
          <FontAwesome
            name="video-camera"
          />
        </Button>
        <Button variant="warning" href="#" className="ms-1 text-white">
          <FontAwesome
            name="ellipsis-h"
          />
        </Button>
      </td>
    </tr>
  )
}

export default PatientList;
