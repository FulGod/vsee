import React from 'react';
import Image from 'next/image'
import logo from '../public/logo.png'
import { Button, Card, Col, Container, Form, Nav, Navbar, NavDropdown, Row, Table } from 'react-bootstrap'
import WaitingRoom from '../features/waitingroom/WaitingRoom'
var FontAwesome = require('react-fontawesome')

export default function Layout({
  children
}: {
  children: React.ReactNode
}) {
  return (
    <>
      <Navbar className="border-bottom">
        <Container>
          <Navbar.Brand href="#home">
            <Image
              src={logo}
              className="d-inline-block align-top"
              height={26}
              width={80}
            />
          </Navbar.Brand>
          <Nav
            className="ms-auto my-2 my-lg-0"
            style={{ maxHeight: '100px' }}
            navbarScroll
          >
            <Nav.Link href="https://toan.vsee.me/test_computer">
              <FontAwesome
                name="desktop"
              /> Test Computer
            </Nav.Link>
            <NavDropdown align="end" title={<><FontAwesome
              name="user"
            /> Toan Nguyen</>}>
              <NavDropdown.Item href="#action/3.1">Profile</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">Logout</NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Container>
      </Navbar>
      <Container>
        <Row className="justify-content-center my-5">
          <Col md="6">
            <WaitingRoom />
          </Col>
        </Row>
        <footer className="text-center"><small>© 2017 <a href="https://vsee.com/" className="text-success">VSee</a></small></footer>
      </Container>
    </>
  )
}